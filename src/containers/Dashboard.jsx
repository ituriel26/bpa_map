import React, { Component } from 'react'
import paper from 'paper'
import { WIDTH, HEIGHT } from '../constants/CANVAS'
import DrawEngine from '../draw-engine/drawEngine'

class Dashboard extends Component {

  de = null;
  componentDidMount() {
    paper.setup('canvas');
    this.de = new DrawEngine()
    window.onkeydown = e => this.de.onKeyDown(e);
  }


  render() {
    return (
      <div>
        <canvas id="canvas" ref="canvas" width={WIDTH} height={HEIGHT}
          onMouseDown={e => this.de.onMouseDown(e)}
          onMouseUp={e => this.de.onMouseUp(e)}
          onMouseMove={e => this.de.onMouseMove(e)}
          onClick={e => this.de.onClick(e)}
        />
        <button onClick={e => this.de.saveMapToFile()}>Save</button>
      </div>
    )
  }
}



export default Dashboard