export default class MapBuilder {
  mapName = ''
  map = {};

  constructor(name) {
    this.map.name = name;
  }

  addObj(typeName, obj) {
    switch (typeName) {
      case 'road': {
        this.addRoad(obj);
      }
    }
  }

  addRoad(obj) {
    if (!this.map.road) {
      this.map.road = [];
    }
    this.map.road.push(obj);
  }

  getJson() {
    return JSON.stringify(this.map);
  }
}