import { Path, Point } from 'paper'

export default class DrawBorderStrategy {
  roadId = 1;
  path = null;
  startPoint = null;

  update = function () { };

  constructor() {
    this.path = new Path();
  }

  onMouseDown(e) {
    if (e.button === 2) {
      this.rightButton(e);
      return;
    }

    this.path.strokeColor = 'red';
    this.startPoint = new Point(e.clientX, e.clientY);
    this.path.moveTo(this.startPoint);
    this.onMouseMove(e);
  }

  onMouseUp(e) {
    if (this.startPoint) {
      const endPoint = new Point(e.clientX, e.clientY);
      this.path.lineTo(endPoint);

      this.update();
      this.startPoint = null;

    }
  }

  onMouseMove(e) {
    if (this.startPoint) {
      const lastSegmentIndex = this.path._segments.length - 1
      this.path.removeSegment(lastSegmentIndex);
      const endPoint = new Point(e.clientX, e.clientY);
      this.path.lineTo(endPoint);
    }
  }

  rightButton(e) {
    console.log(e);
  }
}