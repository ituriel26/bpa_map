import { Path, Point } from 'paper'

export default class DrawRoadStrategy {
  roadId = 1;
  path = null;
  isDrawing = false;
  segmentsCount = 0;

  update = function () { };

  constructor() {
    this.path = new Path();
    this.path.strokeColor = 'red';
  }

  onClick(e) {
    const point = new Point(e.clientX, e.clientY);

    if (this.path._segments.length === 0) {
      this.path.add(point);
      this.isDrawing = true;
    }
    else if (!this.isDrawing && this.path._segments.length > 0) {
      this.path.lineTo(point);
      this.isDrawing = true;
    }
    else {
      const lastSeg = this.path._segments[this.path._segments.length - 2];

      const lineObj = {
        startPoint: {
          x: lastSeg.point.x,
          y: lastSeg.point.y,
        },
        endPoint: {
          x: e.clientX,
          y: e.clientY,
        }
      };

      this.update(this.roadId, lineObj);
    }
    this.segmentsCount++;
  }

  onMouseMove(e) {
    if (this.isDrawing) {
      const lastSegmentIndex = this.path._segments.length - 1;
      if (this.path._segments.length > this.segmentsCount) {
        this.path.removeSegment(lastSegmentIndex);
      }
      const endPoint = new Point(e.clientX, e.clientY);
      this.path.lineTo(endPoint);
    }
  }

  onKeyDown(e) {
    if (e.keyCode === 27) { //esc
      this.isDrawing = false;

      const lastSegmentIndex = this.path._segments.length - 1;
      if (this.path._segments.length > this.segmentsCount) {
        this.path.removeSegment(lastSegmentIndex);
      }
    }
  }
}