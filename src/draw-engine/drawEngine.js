import DrawStrategyFactory from './drawStrategieFactory';
import MapBuilder from './mapBuilder';
import fileDownload from 'js-file-download';

export default class DrawEngine {
  drawStrategy = null;
  drawStrategyFactory = new DrawStrategyFactory();
  mapBuilder = new MapBuilder('newMap');
  currentStrategy = 'road';

  constructor() {
    this.drawStrategy = this.drawStrategyFactory.getDrawStrategy(this.currentStrategy);
    this.drawStrategy.update = (id, obj) => this.update(id, obj);
  }

  update(id, obj) {
    this.mapBuilder.addObj(this.currentStrategy, obj);
  }

  saveMapToFile(filePath) {
    const json = this.mapBuilder.getJson();
    fileDownload(json, 'filename.json');
  }

  onClick(e) {
    if (this.drawStrategy.onClick) {
      this.drawStrategy.onClick(e);
    }
  }

  onMouseDown(e) {
    if (this.drawStrategy.onMouseDown) {
      this.drawStrategy.onMouseDown(e);
    }
  }

  onMouseUp(e) {
    if (this.drawStrategy.onMouseUp) {
      this.drawStrategy.onMouseUp(e);
    }
  }

  onMouseMove(e) {
    if (this.drawStrategy.onMouseMove) {
      this.drawStrategy.onMouseMove(e);
    }
  }

  onKeyDown(e) {
    if (this.drawStrategy.onKeyDown) {
      this.drawStrategy.onKeyDown(e);
    }
  }
}