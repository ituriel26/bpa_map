import DrawRoadStrategy from "./draw-strategies/drawRoadStrategy";

export default class DrawStrategyFactory {
  getDrawStrategy(strategyName) {
    if (strategyName === 'road') {
      return new DrawRoadStrategy();
    }
  }

}